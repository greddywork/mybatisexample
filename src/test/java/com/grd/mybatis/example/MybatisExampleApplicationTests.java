package com.grd.mybatis.example;

import com.grd.mybatis.example.dao.AdmMenuDynamicSqlSupport;
import com.grd.mybatis.example.dao.AdmMenuMapper;
import com.grd.mybatis.example.vo.AdmMenu;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.dynamic.sql.SqlBuilder;
import org.mybatis.dynamic.sql.SqlTable;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MybatisExampleApplicationTests {

    @Autowired
    private AdmMenuMapper admMenuMapper;

    @Test
    public void selectAll1() {
        List<AdmMenu> admMenuList = admMenuMapper.select(SelectDSLCompleter.allRows());
        for(AdmMenu admMenu:admMenuList) {
            System.out.println(admMenu.getMenuName());
        }
    }

    @Test
    public void selectAll2() {
        List<AdmMenu> admMenuList = admMenuMapper.select(c->c);
        for(AdmMenu admMenu:admMenuList) {
            System.out.println(admMenu.getMenuName());
        }
    }

    @Test
    public void selectWhere() {
        List<AdmMenu> admMenuList = admMenuMapper.select(c->c.where(AdmMenuDynamicSqlSupport.consoleType, SqlBuilder.isEqualTo("MER")));
        for(AdmMenu admMenu:admMenuList) {
            System.out.println(admMenu.getMenuName());
        }
    }

    @Test
    public void selectOne() {
        Optional<AdmMenu> admMenu = admMenuMapper.selectOne(c->c.where(AdmMenuDynamicSqlSupport.id, SqlBuilder.isEqualTo(1)));
        if(admMenu.isPresent()) {
            System.out.println(admMenu.get().getMenuName());
        }
    }

    @Test
    public void selectOne2() {
        SelectStatementProvider selectStatementProvider =
                SqlBuilder.select(AdmMenuDynamicSqlSupport.menuName)
                          .from(SqlTable.of("ADM_MENU"))
                          .where(AdmMenuDynamicSqlSupport.id, SqlBuilder.isEqualTo(1))
                          .build()
                          .render(RenderingStrategy.MYBATIS3);
        Optional<AdmMenu> admMenu = admMenuMapper.selectOne(selectStatementProvider);
        if(admMenu.isPresent()) {
            System.out.println(admMenu.get().getMenuName());
        }
    }
}
