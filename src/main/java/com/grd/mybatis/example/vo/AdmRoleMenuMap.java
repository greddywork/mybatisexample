package com.grd.mybatis.example.vo;

import java.util.Date;
import javax.annotation.Generated;

public class AdmRoleMenuMap {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer roleId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer menuId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte createAction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte deleteAction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte updateAction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte queryAction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date createTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer createUserId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date updateTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer updateUserId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getRoleId() {
        return roleId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getMenuId() {
        return menuId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getCreateAction() {
        return createAction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreateAction(Byte createAction) {
        this.createAction = createAction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getDeleteAction() {
        return deleteAction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDeleteAction(Byte deleteAction) {
        this.deleteAction = deleteAction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getUpdateAction() {
        return updateAction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setUpdateAction(Byte updateAction) {
        this.updateAction = updateAction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getQueryAction() {
        return queryAction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setQueryAction(Byte queryAction) {
        this.queryAction = queryAction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getCreateTime() {
        return createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getCreateUserId() {
        return createUserId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getUpdateTime() {
        return updateTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getUpdateUserId() {
        return updateUserId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setUpdateUserId(Integer updateUserId) {
        this.updateUserId = updateUserId;
    }
}