package com.grd.mybatis.example.vo;

import java.util.Date;
import javax.annotation.Generated;

public class AdmMerchant {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String consoleType;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String merchantCode;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String merchantName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte merchantStatus;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date createTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer createUserId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date updateTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer updateUserId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getId() {
        return id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setId(Integer id) {
        this.id = id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getConsoleType() {
        return consoleType;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setConsoleType(String consoleType) {
        this.consoleType = consoleType;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getMerchantCode() {
        return merchantCode;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getMerchantName() {
        return merchantName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getMerchantStatus() {
        return merchantStatus;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMerchantStatus(Byte merchantStatus) {
        this.merchantStatus = merchantStatus;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getCreateTime() {
        return createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getCreateUserId() {
        return createUserId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getUpdateTime() {
        return updateTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getUpdateUserId() {
        return updateUserId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setUpdateUserId(Integer updateUserId) {
        this.updateUserId = updateUserId;
    }
}