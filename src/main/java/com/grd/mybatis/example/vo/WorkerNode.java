package com.grd.mybatis.example.vo;

import java.util.Date;
import javax.annotation.Generated;

public class WorkerNode {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String hostName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String port;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer type;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date launchDate;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date modified;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date created;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getId() {
        return id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getHostName() {
        return hostName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getPort() {
        return port;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setPort(String port) {
        this.port = port;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getType() {
        return type;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setType(Integer type) {
        this.type = type;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getLaunchDate() {
        return launchDate;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setLaunchDate(Date launchDate) {
        this.launchDate = launchDate;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getModified() {
        return modified;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setModified(Date modified) {
        this.modified = modified;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getCreated() {
        return created;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreated(Date created) {
        this.created = created;
    }
}