package com.grd.mybatis.example.vo;

import java.util.Date;
import javax.annotation.Generated;

public class AdmMenu {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String consoleType;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String menuCode;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String menuName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte menuLvl;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer parentMenuId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String menuUrl;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String menuIcon;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte menuSort;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date createTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date updateTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getId() {
        return id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setId(Integer id) {
        this.id = id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getConsoleType() {
        return consoleType;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setConsoleType(String consoleType) {
        this.consoleType = consoleType;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getMenuCode() {
        return menuCode;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getMenuName() {
        return menuName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getMenuLvl() {
        return menuLvl;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMenuLvl(Byte menuLvl) {
        this.menuLvl = menuLvl;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getParentMenuId() {
        return parentMenuId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setParentMenuId(Integer parentMenuId) {
        this.parentMenuId = parentMenuId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getMenuUrl() {
        return menuUrl;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getMenuIcon() {
        return menuIcon;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getMenuSort() {
        return menuSort;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setMenuSort(Byte menuSort) {
        this.menuSort = menuSort;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getCreateTime() {
        return createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getUpdateTime() {
        return updateTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}