package com.grd.mybatis.example.dao;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class AdmMenuDynamicSqlSupport {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final AdmMenu admMenu = new AdmMenu();

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> id = admMenu.id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> consoleType = admMenu.consoleType;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> menuCode = admMenu.menuCode;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> menuName = admMenu.menuName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Byte> menuLvl = admMenu.menuLvl;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> parentMenuId = admMenu.parentMenuId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> menuUrl = admMenu.menuUrl;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> menuIcon = admMenu.menuIcon;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Byte> menuSort = admMenu.menuSort;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> createTime = admMenu.createTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> updateTime = admMenu.updateTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final class AdmMenu extends SqlTable {
        public final SqlColumn<Integer> id = column("id", JDBCType.INTEGER);

        public final SqlColumn<String> consoleType = column("console_type", JDBCType.CHAR);

        public final SqlColumn<String> menuCode = column("menu_code", JDBCType.CHAR);

        public final SqlColumn<String> menuName = column("menu_name", JDBCType.VARCHAR);

        public final SqlColumn<Byte> menuLvl = column("menu_lvl", JDBCType.TINYINT);

        public final SqlColumn<Integer> parentMenuId = column("parent_menu_id", JDBCType.INTEGER);

        public final SqlColumn<String> menuUrl = column("menu_url", JDBCType.VARCHAR);

        public final SqlColumn<String> menuIcon = column("menu_icon", JDBCType.VARCHAR);

        public final SqlColumn<Byte> menuSort = column("menu_sort", JDBCType.TINYINT);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public AdmMenu() {
            super("ADM_MENU");
        }
    }
}