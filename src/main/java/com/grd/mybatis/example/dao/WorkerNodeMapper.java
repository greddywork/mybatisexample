package com.grd.mybatis.example.dao;

import static com.grd.mybatis.example.dao.WorkerNodeDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.grd.mybatis.example.vo.WorkerNode;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface WorkerNodeMapper {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    BasicColumn[] selectList = BasicColumn.columnList(id, hostName, port, type, launchDate, modified, created);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<WorkerNode> insertStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<WorkerNode> multipleInsertStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("WorkerNodeResult")
    Optional<WorkerNode> selectOne(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="WorkerNodeResult", value = {
        @Result(column="ID", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="HOST_NAME", property="hostName", jdbcType=JdbcType.VARCHAR),
        @Result(column="PORT", property="port", jdbcType=JdbcType.VARCHAR),
        @Result(column="TYPE", property="type", jdbcType=JdbcType.INTEGER),
        @Result(column="LAUNCH_DATE", property="launchDate", jdbcType=JdbcType.DATE),
        @Result(column="MODIFIED", property="modified", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="CREATED", property="created", jdbcType=JdbcType.TIMESTAMP)
    })
    List<WorkerNode> selectMany(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, workerNode, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, workerNode, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int deleteByPrimaryKey(Long id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insert(WorkerNode record) {
        return MyBatis3Utils.insert(this::insert, record, workerNode, c ->
            c.map(id).toProperty("id")
            .map(hostName).toProperty("hostName")
            .map(port).toProperty("port")
            .map(type).toProperty("type")
            .map(launchDate).toProperty("launchDate")
            .map(modified).toProperty("modified")
            .map(created).toProperty("created")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertMultiple(Collection<WorkerNode> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, workerNode, c ->
            c.map(id).toProperty("id")
            .map(hostName).toProperty("hostName")
            .map(port).toProperty("port")
            .map(type).toProperty("type")
            .map(launchDate).toProperty("launchDate")
            .map(modified).toProperty("modified")
            .map(created).toProperty("created")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertSelective(WorkerNode record) {
        return MyBatis3Utils.insert(this::insert, record, workerNode, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(hostName).toPropertyWhenPresent("hostName", record::getHostName)
            .map(port).toPropertyWhenPresent("port", record::getPort)
            .map(type).toPropertyWhenPresent("type", record::getType)
            .map(launchDate).toPropertyWhenPresent("launchDate", record::getLaunchDate)
            .map(modified).toPropertyWhenPresent("modified", record::getModified)
            .map(created).toPropertyWhenPresent("created", record::getCreated)
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default Optional<WorkerNode> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, workerNode, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<WorkerNode> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, workerNode, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<WorkerNode> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, workerNode, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default Optional<WorkerNode> selectByPrimaryKey(Long id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, workerNode, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateAllColumns(WorkerNode record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(hostName).equalTo(record::getHostName)
                .set(port).equalTo(record::getPort)
                .set(type).equalTo(record::getType)
                .set(launchDate).equalTo(record::getLaunchDate)
                .set(modified).equalTo(record::getModified)
                .set(created).equalTo(record::getCreated);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(WorkerNode record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(hostName).equalToWhenPresent(record::getHostName)
                .set(port).equalToWhenPresent(record::getPort)
                .set(type).equalToWhenPresent(record::getType)
                .set(launchDate).equalToWhenPresent(record::getLaunchDate)
                .set(modified).equalToWhenPresent(record::getModified)
                .set(created).equalToWhenPresent(record::getCreated);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int updateByPrimaryKey(WorkerNode record) {
        return update(c ->
            c.set(hostName).equalTo(record::getHostName)
            .set(port).equalTo(record::getPort)
            .set(type).equalTo(record::getType)
            .set(launchDate).equalTo(record::getLaunchDate)
            .set(modified).equalTo(record::getModified)
            .set(created).equalTo(record::getCreated)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int updateByPrimaryKeySelective(WorkerNode record) {
        return update(c ->
            c.set(hostName).equalToWhenPresent(record::getHostName)
            .set(port).equalToWhenPresent(record::getPort)
            .set(type).equalToWhenPresent(record::getType)
            .set(launchDate).equalToWhenPresent(record::getLaunchDate)
            .set(modified).equalToWhenPresent(record::getModified)
            .set(created).equalToWhenPresent(record::getCreated)
            .where(id, isEqualTo(record::getId))
        );
    }
}