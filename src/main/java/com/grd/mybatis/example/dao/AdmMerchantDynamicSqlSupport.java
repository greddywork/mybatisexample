package com.grd.mybatis.example.dao;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class AdmMerchantDynamicSqlSupport {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final AdmMerchant admMerchant = new AdmMerchant();

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> id = admMerchant.id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> consoleType = admMerchant.consoleType;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> merchantCode = admMerchant.merchantCode;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> merchantName = admMerchant.merchantName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Byte> merchantStatus = admMerchant.merchantStatus;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> createTime = admMerchant.createTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> createUserId = admMerchant.createUserId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> updateTime = admMerchant.updateTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> updateUserId = admMerchant.updateUserId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final class AdmMerchant extends SqlTable {
        public final SqlColumn<Integer> id = column("id", JDBCType.INTEGER);

        public final SqlColumn<String> consoleType = column("console_type", JDBCType.CHAR);

        public final SqlColumn<String> merchantCode = column("merchant_code", JDBCType.CHAR);

        public final SqlColumn<String> merchantName = column("merchant_name", JDBCType.VARCHAR);

        public final SqlColumn<Byte> merchantStatus = column("merchant_status", JDBCType.TINYINT);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Integer> createUserId = column("create_user_id", JDBCType.INTEGER);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Integer> updateUserId = column("update_user_id", JDBCType.INTEGER);

        public AdmMerchant() {
            super("ADM_MERCHANT");
        }
    }
}