package com.grd.mybatis.example.dao;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class AdmRoleMenuMapDynamicSqlSupport {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final AdmRoleMenuMap admRoleMenuMap = new AdmRoleMenuMap();

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> roleId = admRoleMenuMap.roleId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> menuId = admRoleMenuMap.menuId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Byte> createAction = admRoleMenuMap.createAction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Byte> deleteAction = admRoleMenuMap.deleteAction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Byte> updateAction = admRoleMenuMap.updateAction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Byte> queryAction = admRoleMenuMap.queryAction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> createTime = admRoleMenuMap.createTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> createUserId = admRoleMenuMap.createUserId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> updateTime = admRoleMenuMap.updateTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> updateUserId = admRoleMenuMap.updateUserId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final class AdmRoleMenuMap extends SqlTable {
        public final SqlColumn<Integer> roleId = column("role_id", JDBCType.INTEGER);

        public final SqlColumn<Integer> menuId = column("menu_id", JDBCType.INTEGER);

        public final SqlColumn<Byte> createAction = column("create_action", JDBCType.TINYINT);

        public final SqlColumn<Byte> deleteAction = column("delete_action", JDBCType.TINYINT);

        public final SqlColumn<Byte> updateAction = column("update_action", JDBCType.TINYINT);

        public final SqlColumn<Byte> queryAction = column("query_action", JDBCType.TINYINT);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Integer> createUserId = column("create_user_id", JDBCType.INTEGER);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Integer> updateUserId = column("update_user_id", JDBCType.INTEGER);

        public AdmRoleMenuMap() {
            super("ADM_ROLE_MENU_MAP");
        }
    }
}