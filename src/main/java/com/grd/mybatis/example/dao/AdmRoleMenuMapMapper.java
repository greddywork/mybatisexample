package com.grd.mybatis.example.dao;

import static com.grd.mybatis.example.dao.AdmRoleMenuMapDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.grd.mybatis.example.vo.AdmRoleMenuMap;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface AdmRoleMenuMapMapper {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    BasicColumn[] selectList = BasicColumn.columnList(roleId, menuId, createAction, deleteAction, updateAction, queryAction, createTime, createUserId, updateTime, updateUserId);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<AdmRoleMenuMap> insertStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<AdmRoleMenuMap> multipleInsertStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("AdmRoleMenuMapResult")
    Optional<AdmRoleMenuMap> selectOne(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="AdmRoleMenuMapResult", value = {
        @Result(column="role_id", property="roleId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="menu_id", property="menuId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="create_action", property="createAction", jdbcType=JdbcType.TINYINT),
        @Result(column="delete_action", property="deleteAction", jdbcType=JdbcType.TINYINT),
        @Result(column="update_action", property="updateAction", jdbcType=JdbcType.TINYINT),
        @Result(column="query_action", property="queryAction", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_user_id", property="createUserId", jdbcType=JdbcType.INTEGER),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_user_id", property="updateUserId", jdbcType=JdbcType.INTEGER)
    })
    List<AdmRoleMenuMap> selectMany(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, admRoleMenuMap, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, admRoleMenuMap, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int deleteByPrimaryKey(Integer roleId_, Integer menuId_) {
        return delete(c -> 
            c.where(roleId, isEqualTo(roleId_))
            .and(menuId, isEqualTo(menuId_))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insert(AdmRoleMenuMap record) {
        return MyBatis3Utils.insert(this::insert, record, admRoleMenuMap, c ->
            c.map(roleId).toProperty("roleId")
            .map(menuId).toProperty("menuId")
            .map(createAction).toProperty("createAction")
            .map(deleteAction).toProperty("deleteAction")
            .map(updateAction).toProperty("updateAction")
            .map(queryAction).toProperty("queryAction")
            .map(createTime).toProperty("createTime")
            .map(createUserId).toProperty("createUserId")
            .map(updateTime).toProperty("updateTime")
            .map(updateUserId).toProperty("updateUserId")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertMultiple(Collection<AdmRoleMenuMap> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, admRoleMenuMap, c ->
            c.map(roleId).toProperty("roleId")
            .map(menuId).toProperty("menuId")
            .map(createAction).toProperty("createAction")
            .map(deleteAction).toProperty("deleteAction")
            .map(updateAction).toProperty("updateAction")
            .map(queryAction).toProperty("queryAction")
            .map(createTime).toProperty("createTime")
            .map(createUserId).toProperty("createUserId")
            .map(updateTime).toProperty("updateTime")
            .map(updateUserId).toProperty("updateUserId")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertSelective(AdmRoleMenuMap record) {
        return MyBatis3Utils.insert(this::insert, record, admRoleMenuMap, c ->
            c.map(roleId).toPropertyWhenPresent("roleId", record::getRoleId)
            .map(menuId).toPropertyWhenPresent("menuId", record::getMenuId)
            .map(createAction).toPropertyWhenPresent("createAction", record::getCreateAction)
            .map(deleteAction).toPropertyWhenPresent("deleteAction", record::getDeleteAction)
            .map(updateAction).toPropertyWhenPresent("updateAction", record::getUpdateAction)
            .map(queryAction).toPropertyWhenPresent("queryAction", record::getQueryAction)
            .map(createTime).toPropertyWhenPresent("createTime", record::getCreateTime)
            .map(createUserId).toPropertyWhenPresent("createUserId", record::getCreateUserId)
            .map(updateTime).toPropertyWhenPresent("updateTime", record::getUpdateTime)
            .map(updateUserId).toPropertyWhenPresent("updateUserId", record::getUpdateUserId)
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default Optional<AdmRoleMenuMap> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, admRoleMenuMap, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<AdmRoleMenuMap> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, admRoleMenuMap, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<AdmRoleMenuMap> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, admRoleMenuMap, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default Optional<AdmRoleMenuMap> selectByPrimaryKey(Integer roleId_, Integer menuId_) {
        return selectOne(c ->
            c.where(roleId, isEqualTo(roleId_))
            .and(menuId, isEqualTo(menuId_))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, admRoleMenuMap, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateAllColumns(AdmRoleMenuMap record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(roleId).equalTo(record::getRoleId)
                .set(menuId).equalTo(record::getMenuId)
                .set(createAction).equalTo(record::getCreateAction)
                .set(deleteAction).equalTo(record::getDeleteAction)
                .set(updateAction).equalTo(record::getUpdateAction)
                .set(queryAction).equalTo(record::getQueryAction)
                .set(createTime).equalTo(record::getCreateTime)
                .set(createUserId).equalTo(record::getCreateUserId)
                .set(updateTime).equalTo(record::getUpdateTime)
                .set(updateUserId).equalTo(record::getUpdateUserId);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(AdmRoleMenuMap record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(roleId).equalToWhenPresent(record::getRoleId)
                .set(menuId).equalToWhenPresent(record::getMenuId)
                .set(createAction).equalToWhenPresent(record::getCreateAction)
                .set(deleteAction).equalToWhenPresent(record::getDeleteAction)
                .set(updateAction).equalToWhenPresent(record::getUpdateAction)
                .set(queryAction).equalToWhenPresent(record::getQueryAction)
                .set(createTime).equalToWhenPresent(record::getCreateTime)
                .set(createUserId).equalToWhenPresent(record::getCreateUserId)
                .set(updateTime).equalToWhenPresent(record::getUpdateTime)
                .set(updateUserId).equalToWhenPresent(record::getUpdateUserId);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int updateByPrimaryKey(AdmRoleMenuMap record) {
        return update(c ->
            c.set(createAction).equalTo(record::getCreateAction)
            .set(deleteAction).equalTo(record::getDeleteAction)
            .set(updateAction).equalTo(record::getUpdateAction)
            .set(queryAction).equalTo(record::getQueryAction)
            .set(createTime).equalTo(record::getCreateTime)
            .set(createUserId).equalTo(record::getCreateUserId)
            .set(updateTime).equalTo(record::getUpdateTime)
            .set(updateUserId).equalTo(record::getUpdateUserId)
            .where(roleId, isEqualTo(record::getRoleId))
            .and(menuId, isEqualTo(record::getMenuId))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int updateByPrimaryKeySelective(AdmRoleMenuMap record) {
        return update(c ->
            c.set(createAction).equalToWhenPresent(record::getCreateAction)
            .set(deleteAction).equalToWhenPresent(record::getDeleteAction)
            .set(updateAction).equalToWhenPresent(record::getUpdateAction)
            .set(queryAction).equalToWhenPresent(record::getQueryAction)
            .set(createTime).equalToWhenPresent(record::getCreateTime)
            .set(createUserId).equalToWhenPresent(record::getCreateUserId)
            .set(updateTime).equalToWhenPresent(record::getUpdateTime)
            .set(updateUserId).equalToWhenPresent(record::getUpdateUserId)
            .where(roleId, isEqualTo(record::getRoleId))
            .and(menuId, isEqualTo(record::getMenuId))
        );
    }
}