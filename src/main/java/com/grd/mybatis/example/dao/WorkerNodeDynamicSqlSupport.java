package com.grd.mybatis.example.dao;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class WorkerNodeDynamicSqlSupport {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final WorkerNode workerNode = new WorkerNode();

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Long> id = workerNode.id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> hostName = workerNode.hostName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> port = workerNode.port;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Integer> type = workerNode.type;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> launchDate = workerNode.launchDate;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> modified = workerNode.modified;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> created = workerNode.created;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final class WorkerNode extends SqlTable {
        public final SqlColumn<Long> id = column("ID", JDBCType.BIGINT);

        public final SqlColumn<String> hostName = column("HOST_NAME", JDBCType.VARCHAR);

        public final SqlColumn<String> port = column("PORT", JDBCType.VARCHAR);

        public final SqlColumn<Integer> type = column("TYPE", JDBCType.INTEGER);

        public final SqlColumn<Date> launchDate = column("LAUNCH_DATE", JDBCType.DATE);

        public final SqlColumn<Date> modified = column("MODIFIED", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> created = column("CREATED", JDBCType.TIMESTAMP);

        public WorkerNode() {
            super("WORKER_NODE");
        }
    }
}