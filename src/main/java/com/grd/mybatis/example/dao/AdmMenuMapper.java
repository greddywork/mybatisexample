package com.grd.mybatis.example.dao;

import static com.grd.mybatis.example.dao.AdmMenuDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.grd.mybatis.example.vo.AdmMenu;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface AdmMenuMapper {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    BasicColumn[] selectList = BasicColumn.columnList(id, consoleType, menuCode, menuName, menuLvl, parentMenuId, menuUrl, menuIcon, menuSort, createTime, updateTime);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<AdmMenu> insertStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<AdmMenu> multipleInsertStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("AdmMenuResult")
    Optional<AdmMenu> selectOne(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="AdmMenuResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="console_type", property="consoleType", jdbcType=JdbcType.CHAR),
        @Result(column="menu_code", property="menuCode", jdbcType=JdbcType.CHAR),
        @Result(column="menu_name", property="menuName", jdbcType=JdbcType.VARCHAR),
        @Result(column="menu_lvl", property="menuLvl", jdbcType=JdbcType.TINYINT),
        @Result(column="parent_menu_id", property="parentMenuId", jdbcType=JdbcType.INTEGER),
        @Result(column="menu_url", property="menuUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="menu_icon", property="menuIcon", jdbcType=JdbcType.VARCHAR),
        @Result(column="menu_sort", property="menuSort", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<AdmMenu> selectMany(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, admMenu, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, admMenu, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int deleteByPrimaryKey(Integer id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insert(AdmMenu record) {
        return MyBatis3Utils.insert(this::insert, record, admMenu, c ->
            c.map(id).toProperty("id")
            .map(consoleType).toProperty("consoleType")
            .map(menuCode).toProperty("menuCode")
            .map(menuName).toProperty("menuName")
            .map(menuLvl).toProperty("menuLvl")
            .map(parentMenuId).toProperty("parentMenuId")
            .map(menuUrl).toProperty("menuUrl")
            .map(menuIcon).toProperty("menuIcon")
            .map(menuSort).toProperty("menuSort")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertMultiple(Collection<AdmMenu> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, admMenu, c ->
            c.map(id).toProperty("id")
            .map(consoleType).toProperty("consoleType")
            .map(menuCode).toProperty("menuCode")
            .map(menuName).toProperty("menuName")
            .map(menuLvl).toProperty("menuLvl")
            .map(parentMenuId).toProperty("parentMenuId")
            .map(menuUrl).toProperty("menuUrl")
            .map(menuIcon).toProperty("menuIcon")
            .map(menuSort).toProperty("menuSort")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertSelective(AdmMenu record) {
        return MyBatis3Utils.insert(this::insert, record, admMenu, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(consoleType).toPropertyWhenPresent("consoleType", record::getConsoleType)
            .map(menuCode).toPropertyWhenPresent("menuCode", record::getMenuCode)
            .map(menuName).toPropertyWhenPresent("menuName", record::getMenuName)
            .map(menuLvl).toPropertyWhenPresent("menuLvl", record::getMenuLvl)
            .map(parentMenuId).toPropertyWhenPresent("parentMenuId", record::getParentMenuId)
            .map(menuUrl).toPropertyWhenPresent("menuUrl", record::getMenuUrl)
            .map(menuIcon).toPropertyWhenPresent("menuIcon", record::getMenuIcon)
            .map(menuSort).toPropertyWhenPresent("menuSort", record::getMenuSort)
            .map(createTime).toPropertyWhenPresent("createTime", record::getCreateTime)
            .map(updateTime).toPropertyWhenPresent("updateTime", record::getUpdateTime)
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default Optional<AdmMenu> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, admMenu, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<AdmMenu> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, admMenu, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<AdmMenu> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, admMenu, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default Optional<AdmMenu> selectByPrimaryKey(Integer id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, admMenu, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateAllColumns(AdmMenu record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(consoleType).equalTo(record::getConsoleType)
                .set(menuCode).equalTo(record::getMenuCode)
                .set(menuName).equalTo(record::getMenuName)
                .set(menuLvl).equalTo(record::getMenuLvl)
                .set(parentMenuId).equalTo(record::getParentMenuId)
                .set(menuUrl).equalTo(record::getMenuUrl)
                .set(menuIcon).equalTo(record::getMenuIcon)
                .set(menuSort).equalTo(record::getMenuSort)
                .set(createTime).equalTo(record::getCreateTime)
                .set(updateTime).equalTo(record::getUpdateTime);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(AdmMenu record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(consoleType).equalToWhenPresent(record::getConsoleType)
                .set(menuCode).equalToWhenPresent(record::getMenuCode)
                .set(menuName).equalToWhenPresent(record::getMenuName)
                .set(menuLvl).equalToWhenPresent(record::getMenuLvl)
                .set(parentMenuId).equalToWhenPresent(record::getParentMenuId)
                .set(menuUrl).equalToWhenPresent(record::getMenuUrl)
                .set(menuIcon).equalToWhenPresent(record::getMenuIcon)
                .set(menuSort).equalToWhenPresent(record::getMenuSort)
                .set(createTime).equalToWhenPresent(record::getCreateTime)
                .set(updateTime).equalToWhenPresent(record::getUpdateTime);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int updateByPrimaryKey(AdmMenu record) {
        return update(c ->
            c.set(consoleType).equalTo(record::getConsoleType)
            .set(menuCode).equalTo(record::getMenuCode)
            .set(menuName).equalTo(record::getMenuName)
            .set(menuLvl).equalTo(record::getMenuLvl)
            .set(parentMenuId).equalTo(record::getParentMenuId)
            .set(menuUrl).equalTo(record::getMenuUrl)
            .set(menuIcon).equalTo(record::getMenuIcon)
            .set(menuSort).equalTo(record::getMenuSort)
            .set(createTime).equalTo(record::getCreateTime)
            .set(updateTime).equalTo(record::getUpdateTime)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int updateByPrimaryKeySelective(AdmMenu record) {
        return update(c ->
            c.set(consoleType).equalToWhenPresent(record::getConsoleType)
            .set(menuCode).equalToWhenPresent(record::getMenuCode)
            .set(menuName).equalToWhenPresent(record::getMenuName)
            .set(menuLvl).equalToWhenPresent(record::getMenuLvl)
            .set(parentMenuId).equalToWhenPresent(record::getParentMenuId)
            .set(menuUrl).equalToWhenPresent(record::getMenuUrl)
            .set(menuIcon).equalToWhenPresent(record::getMenuIcon)
            .set(menuSort).equalToWhenPresent(record::getMenuSort)
            .set(createTime).equalToWhenPresent(record::getCreateTime)
            .set(updateTime).equalToWhenPresent(record::getUpdateTime)
            .where(id, isEqualTo(record::getId))
        );
    }
}