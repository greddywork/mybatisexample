package com.grd.mybatis.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
@MapperScan(basePackages = {"com.grd.mybatis.example.dao"})
public class MybatisExampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisExampleApplication.class, args);
    }
}